require 'fontastic2sass/version'
require 'fontastic2sass/archive'
require 'fontastic2sass/zip'
require 'fontastic2sass/dir'
require 'fontastic2sass/font'
require 'fontastic2sass/sass'
require 'fontastic2sass/utilities'
require 'hash'


module Fontastic2Sass

  def self.run!(source, font_path, sass_path, options = {})
    defaults = {
      scss: false,
      compatible: false,
      oocss: false,
      demo_path: options[:"demo-path"]
    }

    options = defaults.merge options.symbolize_keys

    utilities = Fontastic2Sass::Utilities.new

    if source.end_with? '.zip'
      files = Fontastic2Sass::Zip.new source

    elsif ::Dir.exists? source
      files = Fontastic2Sass::Dir.new source

    else
      raise 'Source must be either a directory or .zip file!'

    end

    # raise 'Source must contain \'selection.json\'.' unless files.files['selection.json']

    font = Fontastic2Sass::Font.new files.metadata_file

    syntax = options[:scss] ? 'scss' : 'sass'

    compatible = options[:compatible] || false

    sass = Fontastic2Sass::Sass.new font, syntax, compatible


    # Save the Sass file
    utilities.create_file "#{sass_path}/_icons.#{sass.syntax}", sass.code

    if options[:oocss]
      utilities.create_file "#{sass_path}/_oocss_icons.#{sass.syntax}", sass.oocss
    end

    files.font_files.each do |filename, content|
      utilities.create_file "#{font_path}/#{filename.sub('fonts/', '')}", content
    end

    if options[:demo_path]
      files.demo_files.each do |filename, content|
        utilities.create_file "#{options[:demo_path]}/#{filename}", content
      end

      files.font_files.each do |filename, content|
        utilities.create_file "#{options[:demo_path]}/demo-files/fonts/#{filename.sub('fonts/', '')}", content
      end

      utilities.gsub_file "#{options[:demo_path]}/icons-reference.html", /href="styles.css">/, 'href="demo-files/styles.css">'
    end
  end

end

