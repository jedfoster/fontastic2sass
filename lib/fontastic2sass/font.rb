class Fontastic2Sass::Font
  attr_reader :font_family

  def initialize(file)
    @font_family = file.scan(/font-family\: \"(.+)\"/).first.first

    @raw_icons = file.scan(/\.icon-(.+)\:.+{\s+content\: \"(.+)\"/)
  end

  def icons
    @icons ||= begin
      icons = {}

      @raw_icons.each do |icon|
        icons[icon.first] = {
          codepoint: icon.last.include?('\\') ? icon.last :  '\%0x' % icon.last.codepoints.first.ord
        }
      end

      icons
    end
  end
end

