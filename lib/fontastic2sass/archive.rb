class Fontastic2Sass::Archive
  attr_reader :files

  EXTRACTABLE_PATTERN = /(fonts\/.*|styles\.css|icons-reference\.html)/

  def font_files
    @_font_files ||= @files.reject {|key| !key.match(/fonts/) }
  end

  def metadata_file
    @_metadata_file ||= @files.reject {|key| !key.match(/styles.css/) }.values.first
  end

  def demo_files
    @_demo_files ||= begin
      demo_files = @files.reject {|key| !key.match(/(\.html|\.css)/) }
      demo_files['demo-files/styles.css'] = demo_files.delete('styles.css')
      demo_files
    end
  end

end

